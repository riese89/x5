import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static String repeatEnter = ". Введите нужные значения заново.";
    public static void main(String[] args) {
        Double[] values = enterValues("Введите N и R", 2);
        Integer countNumbers = values[0].intValue();
        Double radius = values[1];
        Double[] points = null;
        if (countNumbers != 0) {
            points = enterValues("Введите" + endPoints(countNumbers) + " подачи через пробел:", countNumbers);
        }
        System.out.println("Количество рекомендованных точек:");
        Integer count = countPoints(countNumbers, radius, points);
        System.out.println(count);
    }

    public static Integer countPoints (Integer countNumbers, Double radius, Double[] points)    {
        Double[] resultArray = null;
        int count = 0;
        Arrays.sort(points);
        for (int i = 0; i < countNumbers; i++) {
            int finalI = i;
            Double[] now = Arrays.stream(points).filter(p -> (p >= points[finalI] - radius && p <= points[finalI] + radius)).toArray(Double[]::new);
            if (resultArray == null) {
                count++;
                resultArray = now;
                continue;
            }
            int compare = compareArrays(resultArray, now);
            if (compare == 1 || compare == 2 || compare == 4) {
                continue;
            }
            if (compare == 3) {
                resultArray = now;
                continue;
            }

            resultArray = addToArray(resultArray, now);
            count++;
        }
        if (resultArray != null && points.length != resultArray.length)    {
            count = count + points.length - resultArray.length;
        }
        return count;
    }

    private static Double[] enterValues(String msg, Integer count) {
        Double[] values2 = null;
        boolean flag = true;
        while (flag) {
            System.out.println(msg);
            String[] stringNumbers = enterLine();
            if (checkCountEnter(count, stringNumbers)) {
                continue;
            }
            values2 = new Double[stringNumbers.length];
            try {
                if (addPointsToArray(values2,stringNumbers,msg)) {
                    continue;
                }
            } catch (Exception ex) {
                System.out.println("Неправильный ввод (вы могли ввести буквы) или вы ввели слишком маленькое/большое число");
                continue;
            }
            flag = false;
        }
        return values2;
    }

    private static String[] enterLine() {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        line = line.replace(',','.');
        String[] result = line.split("[ |\t]");
        result = Arrays.stream(result).filter(r -> !r.equals("")).toArray(String[]::new);
        return result;
    }

    private static int compareArrays(Double[] prev, Double[] now) {
        //0 - разные
        //1 - равны
        //2 - prev включает now
        //3 - now включает prev
        //4 - есть пересечения
        Arrays.sort(prev);
        Arrays.sort(now);
        if (Arrays.equals(prev, now)) {
            return 1;
        }
        if (includeArrays(prev, now) && prev.length > now.length) {
            return 2;
        }
        if (includeArrays(now, prev)) {
            return 3;
        }
        if (countUnion(now, prev) > 0) {
            return 4;
        }
        return 0;
    }

    private static boolean includeArrays(Double[] ar1, Double[] ar2) {
        int countIncludes = countUnion(ar1, ar2);
        return countIncludes == ar2.length;
    }

    private static int countUnion(Double[] ar1, Double[] ar2) {
        int countIncludes = 0;
        for (Double value : ar1) {
            for (Double aDouble : ar2) {
                if (value.intValue() == aDouble.intValue()) {
                    countIncludes++;
                }
            }
        }
        return countIncludes;
    }

    private static Double[] addToArray(Double[] nowResArray, Double[] addArray) {
        Double[] result = new Double[nowResArray.length + addArray.length];
        int j = 0;
        for (int i = 0; i < result.length; i++) {
            if (i < nowResArray.length) {
                result[i] = nowResArray[i];
            } else {
                result[i] = addArray[j];
                j++;
            }
        }
        return result;
    }

    public static boolean checkCountEnter(int count, String[] stringNumbers) {
        if (stringNumbers.length != count) {
            if (stringNumbers.length > count) {
                System.out.println("Введено более " + count + " чисел" + repeatEnter);
            } else {
                System.out.println("Введено менее " + count + " чисел" + repeatEnter);
            }
            return true;
        }
        return false;
    }

    private static boolean checkRadius  (Double value) {
        if (value < 0 || value > Math.pow(10,9))  {
            System.out.println("Радиус должен быть не меньше 0 и не больше 10^9" + repeatEnter);
            return true;
        }
        return false;
    }

    private static boolean checkN   (Double value) {
        if (value < 0 || value > Math.pow(10, 5)) {
            System.out.println("Число точек должно быть не меньше 0 и не больше 10^5" + repeatEnter);
            return true;
        }
        if (value % 1 != 0) {
            System.out.println("Число точек должно быть целым" + repeatEnter);
            return true;
        }
        return false;
    }

    private static boolean checkPoints  (Double value)  {
        if (value > Math.pow(10, 9))    {
            System.out.println("Координата точки должна быть не больше 10^5" + repeatEnter);
            return true;
        }
        return false;
    }

    private static String endPoints (Integer count) {
        int lastDigit = count%10;
        int lastTwoDigits = count %100;
        if (check05789(lastDigit) ||
                (lastTwoDigits > 10 && lastTwoDigits <= 19)) {
            return " " + count.toString() + " точек";
        }
        if (check1(lastDigit)) {
            return " " + count.toString() + " точка";
        }
        if (check234(lastDigit)) {
            return " " + count.toString() + " точки";
        }
        return "";
    }

    private static boolean check05789(int lastDigit) {
        return lastDigit == 0 || lastDigit == 5 || lastDigit == 6 || lastDigit == 7 || lastDigit == 8 || lastDigit == 9;
    }

    private static boolean check1(int lastDigit) {
        return lastDigit == 1;
    }

    private static boolean check234(int lastDigit) {
        return lastDigit == 2 || lastDigit == 3 || lastDigit == 4;
    }

    private static boolean addPointsToArray(Double[] values, String[] stringNumbers, String msg)  {
        for (int i = 0; i < stringNumbers.length; i++) {
            values[i] = Double.parseDouble(stringNumbers[i]);
            if (msg.equals("Введите N и R") &&
                    (( i == 0 && checkN(values[i]) )||
                            ( i == 1 && checkRadius(values[i])))) {
                return true;
            }
            if (!msg.equals("Введите N и R") && checkPoints(values[i])) {
                return true;
            }
        }
        return false;
    }
}
