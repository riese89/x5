import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        OperationSeconds os = new OperationSeconds(line);
        try {
            os.setSecondsLine(os.parse());
            Long secondsLine = os.getSecondsLine();
            if (secondsLine == null) {
                throw new IllegalArgumentException(": не могу преобразовать '" + line + "' в число");
            }
            if(secondsLine < 0) {
                throw new IllegalArgumentException(": число должно быть больше 0");
            }
            Long hours = os.getHours();
            Long minutes = os.getMinutes();
            System.out.println("Перевод секунд в минуты и часы");
            System.out.println(hours + os.endHours() + minutes + os.endMinutes());
            System.out.println("...");
        } catch (Exception ex) {
            System.out.println("Ошибка: " + ex.getClass().getCanonicalName() + ex.getMessage());
        }
    }
}