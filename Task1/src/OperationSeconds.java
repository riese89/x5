public class OperationSeconds {

    private Long secondsLine;
    private String line;
    private Long hours;
    private Long minutes;

    public Long getSecondsLine() {
        return secondsLine;
    }

    public Long getHours() {
        setHours();
        return hours;
    }

    public void setHours() {
        hours = secondsLine / 3600;
    }

    public Long getMinutes() {
        setMinutes();
        return minutes;
    }

    public void setMinutes() {
        minutes = (secondsLine - hours * 3600) / 60;
    }

    public OperationSeconds(String line) {
        this.line = line;
    }

    private int getLastDigit(Long number) {
        return (int) (number % 10);
    }

    public void setSecondsLine(Long secondsLine) {
        this.secondsLine = secondsLine;
    }

    public String endHours() {
        int lastDigit = getLastDigit(hours);
        int lastTwoDigits = getLastTwoDigits(hours);
        if (check05789(lastDigit) ||
                (lastTwoDigits > 10 && lastTwoDigits <= 19)) {
            return " часов ";
        }
        if (check1(lastDigit)) {
            return " час ";
        }
        if (check234(lastDigit)) {
            return " часа ";
        }
        return "";
    }

    public String endMinutes() {
        int lastDigit = getLastDigit(minutes);
        if (check05789(lastDigit) || check1019(minutes)) {
            return " минут ";
        }
        if (check1(lastDigit)) {
            return " минута ";
        }
        if (check234(lastDigit)) {
            return " минуты ";
        }
        return "";
    }

    private boolean check05789(int lastDigit) {
        if (lastDigit == 0 || lastDigit == 5 || lastDigit == 6 || lastDigit == 7 || lastDigit == 8 || lastDigit == 9) {
            return true;
        }
        return false;
    }

    private boolean check1(int lastDigit) {
        if (lastDigit == 1) {
            return true;
        }
        return false;
    }

    private boolean check234(int lastDigit) {
        if (lastDigit == 2 || lastDigit == 3 || lastDigit == 4) {
            return true;
        }
        return false;
    }

    private boolean check1019(Long lastDigits) {
        if (lastDigits > 10 && lastDigits <= 19) {
            return true;
        }
        return false;
    }

    private int getLastTwoDigits(Long number) {
        return (int) (number % 100);
    }

    public Long parse() {
        try {
            return Long.parseLong(line);
        } catch (Exception ex) {
            return null;
        }
    }
}
