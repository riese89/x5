import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        boolean flag = true;
        Checkers check = new Checkers();
        char mathSymb;
        System.out.println("Программа выполняет арифметические выражения над действительными\n" +
                "числами и поддерживает следующие операции: +, -, *, /.\n" +
                "Для завершения работы программы введите \"exit\" [Enter].");
        while (flag) {
            System.out.print("\nВведите выражение и нажмите [Enter]: ");
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            if (line.equals("exit")) {
                flag = false;
                continue;
            }
            line = line.replace(" ", ""); //убираем пробелы, табуляцию
            line = line.replace("\t", "");
            line = line.replace(',', '.'); // заменяем , на . , чтобы 2,5 распознавалось как 2.5

            if (check.wrongCombination(line)) {
                continue;
            }
            line = line.replaceFirst("--", "+");
            if (check.checkFirstAndLastSymbols(line) || check.checkCountMathSymbols(line)) { //первый символ цифра или -, последний только цифра
                continue;
            }
            int limit = 2;
            if (line.indexOf('-') == 0) {
                limit = 3;
            }
            String[] partLine = line.split("[*\\- | /\\-/ | * | + | \\-]", limit);
            partLine = Arrays.stream(partLine).filter(pl -> !pl.equals("")).toArray(String[]::new);
            Double[] numbers = check.checkEnterDouble(partLine);
            if (partLine.length != 2 || numbers == null) {
                System.out.println("Неправильный ввод!");
                continue;
            }
            Double number1 = numbers[0];
            Double number2 = numbers[1];
            if (line.indexOf('-') == 0) {
                number1 *= -1;
            }
            int indexOperator = searchMathOperator(line);
            if (indexOperator != -1) {
                mathSymb = line.charAt(indexOperator);

            } else {
                mathSymb = '-';
            }
            Double result;
            try {
                result = operation(mathSymb, number1, number2);
            }
            catch (Exception ex)    {
                System.out.println("Неправильный ввод!");
                continue;
            }
            if (result != null) {
                System.out.println("Ответ: " + result);
            } else {
                System.out.println("Деление на ноль");
            }
        }
        Thread.sleep(150);
        System.out.println("До свидания");
    }
    private static int searchMathOperator(String line) {
        String[] operators = {"+", "/", "*"};
        for (String operator : operators) {
            int index = line.indexOf(operator);
            if (index > -1) {
                return index;
            }
        }
        return -1;
    }

    private static Double operation(char mathSymb, double number1, double number2) {
        switch (mathSymb) {
            case '-': {
                return number1 - number2;
            }
            case '+': {
                return number1 + number2;
            }
            case '/': {
                if (number2 != 0) {
                    return number1 / number2;
                }
                return null;
            }
            case '*': {
                return number1 * number2;
            }
        }
        return null;
    }


}
