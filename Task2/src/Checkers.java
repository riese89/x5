public class Checkers {
    public boolean wrongCombination (String line)    {
        String[] combinations = {"---", "-+"};
        for (String combination : combinations) {
            if (line.contains(combination)) {
                System.out.println("Неправильный ввод");
                return true;
            }
        }
        return false;
    }

    public boolean checkFirstAndLastSymbols(String line) {
        char firstSymbol = line.charAt(0);
        char lastSymbol = line.charAt(line.length() - 1);
        int[] codeSymbols = {(int) firstSymbol, (int) lastSymbol};
        for (int i = 0; i < codeSymbols.length; i++)    {
            if ((codeSymbols[i] < 48 || codeSymbols[i] > 57) && codeSymbols[i] != 44 && codeSymbols[i] != 45 && codeSymbols[i] != 46) {
                if (i == 0) {
                    System.out.println("Неправильный ввод первого символа!");
                }
                else    {
                    System.out.println("Неправильный ввод последнего символа!");
                }
                return true;
            }
        }
        return false;
    }

    public boolean checkCountMathSymbols (String line)   {
        if (countSymb(line, '+') > 1 || countSymb(line, '/') > 1 || countSymb(line, '*') > 1 ||
                (line.charAt(0) == '-' && countSymb(line, '-') > 2) || (line.charAt(0) != '-' && countSymb(line, '-') > 1)) {
            System.out.println("Неправильный ввод"); //смотрим, чтобы в строке было нужное число матсимволов
            return true;
        }
        return false;
    }

    public Double[] checkEnterDouble (String[] line)   {
        Double[] numbers = new Double[2];
        try {
            numbers[0] = Double.parseDouble(line[0]);
            numbers[1] = Double.parseDouble(line[1]);
            return numbers;
        } catch (Exception ex) {
            return null;
        }
    }

    private long countSymb(String line, char symb) {
        return line.chars().filter(ch -> ch == symb).count();
    }

}
